Code Kata the cube
-------------------------------

Un reducido y heterogéneo grupo de personas se ve atrapada en un extraño recinto formado por habitaciones cúbicas interconectadas.

Algunas habitaciones tienen trampas mortales, mientras que otras son seguras.

En la entrada de cada habitación, hay una secuencia de tres números de tres dígitos (entre 000 y 999)

566 472 737

Descubren que las trampas están en aquellas habitaciones en las que alguno de los tres números es la potencia de un primo.

Ejemplos:

149 419 568 -> Trampa
517 478 565 -> Segura
476 804 939 -> Segura

Objetivo: Detectar si alguno de los 3 números es potencia de un solo primo.
(Calcular los factores primos de un numero)

Para la habitación 149 512 568:

149 -> 149 (primo)
512 -> 2^9
568 -> 2^3, 71

NOTAS: Consideramos que 1 no es primo. Un numero primo es potencia de si mismos de forma que representa una sala no segura, con trampa.

-------------------------------------------------------------

A small and heterogeneous group of people trapped in a strange cube enclosure formed by interconnecting rooms. 

Some rooms are death traps, while others are safe. 

At the entrance of each room, there is a sequence of three three-digit numbers (from 000 to 999) 

566 472 737 

They discover that the traps are in those rooms in which one of the three numbers is the power of a prime. 

Examples: 

149 419 568 -> Trap 
517 478 565 -> Trap free 
476 804 939 -> Trap free 

Purpose: Determine if any of the 3 numbers is the power of a single prime. 
(Calculate the prime factors of a number) 

Example room: 149 512 568: 

149 -> 149 (prime) 
512 -> 2 ^ 9 
568 -> 2 ^ 3, 71 

NOTES: We believe 1 is not prime. A prime number is a power of its own so it represents an unsecured room with trap.