package com.lordudun.codingdojo.thecube.doorrules;

import com.lordudun.codingdojo.thecube.CubeUnitTest;
import com.lordudun.codingdojo.thecube.SafeDoorChecker;
import com.lordudun.codingdojo.thecube.doorrules.EveryDoorNumberIsSafeRule;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

public class EveryDoorNumberIsSafeRuleTest extends CubeUnitTest {

    @Mock
    private EveryDoorNumberIsSafeRule mockedRule;

    @Before
    public void initTest() {
        MockitoAnnotations.initMocks(this);
        safeDoorChecker = new SafeDoorChecker(mockedRule);
    }

    @Test
    public void is_safe_door() {
        when(mockedRule.isSafe(any(int[].class))).thenReturn(true);
        assertSafeRoom(any(int[].class));
    }

    @Test
    public void is_room_with_trap() {
        when(mockedRule.isSafe(any(int[].class))).thenReturn(false);
        assertRoomWithTrap(any(int[].class));
    }
}
