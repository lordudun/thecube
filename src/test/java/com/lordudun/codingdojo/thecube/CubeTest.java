package com.lordudun.codingdojo.thecube;

import com.lordudun.codingdojo.thecube.doorrules.EveryDoorNumberIsSafeRule;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CubeTest extends CubeUnitTest {

    @Before
    public void initTest() {
        safeDoorChecker = new SafeDoorChecker(new EveryDoorNumberIsSafeRule());
    }

    @Test
    public void no_door_number_is_prime() {
        assertSafeRoom(1, 1, 1);
    }

    @Test
    public void all_door_number_is_prime() {
        assertRoomWithTrap(2, 2, 2);
    }

    @Test
    public void first_door_number_is_not_prime() {
        assertRoomWithTrap(1,2,2);
    }

    @Test
    public void last_door_number_is_prime() {
        assertRoomWithTrap(1,1,2);
    }

    @Test
    public void first_door_number_is_prime_3() {
        assertRoomWithTrap(3,1,1);
    }

    @Test
    public void first_door_number_is_prime_5() {
        assertRoomWithTrap(5,1,1);
    }

    @Test
    public void first_door_number_is_prime_97() {
        assertRoomWithTrap(97,1,1);
    }

    @Test
    public void first_door_number_potency_of_prime_2() {
        assertRoomWithTrap(4, 1, 1);
    }

    @Test
    public void first_door_number_potency_of_prime_3() {
        assertRoomWithTrap(9, 1, 1);
    }
}
