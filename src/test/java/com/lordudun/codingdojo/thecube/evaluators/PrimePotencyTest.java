package com.lordudun.codingdojo.thecube.evaluators;

import com.lordudun.codingdojo.thecube.evaluators.PrimePotencyEvaluator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PrimePotencyTest {

    private PrimePotencyEvaluator primeCalculator;

    @Before
    public void initTes() {
        primeCalculator = new PrimePotencyEvaluator();
    }

    @Test
    public void one_is_not_prime_potency() {
        assertFalse(primeCalculator.isPrimePotencyNumber(1));
    }

    @Test
    public void six_is_not_prime_potency() {
        assertFalse(primeCalculator.isPrimePotencyNumber(6));
    }

    @Test
    public void ninety_seven_is_prime() {
        assertTrue(primeCalculator.isPrimePotencyNumber(97));
    }

    @Test
    public void nine_is_prime_potency() {
        assertTrue(primeCalculator.isPrimePotencyNumber(9));
    }
}
