package com.lordudun.codingdojo.thecube;

import static org.junit.Assert.assertEquals;

public abstract class CubeUnitTest {

    protected SafeDoorChecker safeDoorChecker;

    protected void assertSafeRoom(int firstNumber, int secondNumber, int thirdNumber) {
        convertAndAssertReturnValue(firstNumber, secondNumber, thirdNumber, SafeDoorChecker.SAFE_ROOM);
    }

    protected void assertSafeRoom(int numbers[]) {
        assertReturnValue(numbers, SafeDoorChecker.SAFE_ROOM);
    }

    protected void assertRoomWithTrap(int firstNumber, int secondNumber, int thirdNumber) {
        convertAndAssertReturnValue(firstNumber, secondNumber, thirdNumber, SafeDoorChecker.ROOM_WITH_TRAP);
    }

    protected void assertRoomWithTrap(int numbers[]) {
        assertReturnValue(numbers, SafeDoorChecker.ROOM_WITH_TRAP);
    }

    private void convertAndAssertReturnValue(int firstNumber, int secondNumber, int thirdNumber, String expected) {
        int[] roomNumbers = convertToVector(firstNumber, secondNumber, thirdNumber);
        assertReturnValue(roomNumbers, expected);
    }

    private void assertReturnValue(int[] roomNumbers, String expected) {

        String result = safeDoorChecker.isSafeRoom(roomNumbers);
        assertEquals(expected, result);
    }

    private int[] convertToVector(int firstNumber, int secondNumber, int thirdNumber) {
        int[] roomNumbers = {firstNumber, secondNumber, thirdNumber};
        return roomNumbers;
    }
}
