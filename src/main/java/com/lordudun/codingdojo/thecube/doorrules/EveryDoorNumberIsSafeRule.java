package com.lordudun.codingdojo.thecube.doorrules;

import com.lordudun.codingdojo.thecube.evaluators.PrimePotencyEvaluator;
import com.lordudun.codingdojo.thecube.SafeDoorRule;

public class EveryDoorNumberIsSafeRule implements SafeDoorRule {

    private PrimePotencyEvaluator safeNumberRule;

    public EveryDoorNumberIsSafeRule() {
        safeNumberRule = new PrimePotencyEvaluator();
    }

    public boolean isSafe(int[] roomNumbers) {
        boolean safeRoom = true;

        for(int nextNumberToCheck : roomNumbers) {
            safeRoom = safeRoom && isSafeNumber(nextNumberToCheck);
        }
        return safeRoom;
    }

    private boolean isSafeNumber(int number) {
        return !safeNumberRule.isPrimePotencyNumber(number);
    }
}
