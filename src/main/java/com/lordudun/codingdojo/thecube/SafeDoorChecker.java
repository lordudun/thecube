package com.lordudun.codingdojo.thecube;

public class SafeDoorChecker {

    protected static final String SAFE_ROOM = "Segura";
    protected static final String ROOM_WITH_TRAP = "Trampa";

    private SafeDoorRule rule;

    public SafeDoorChecker(SafeDoorRule rule) {
        this.rule = rule;
    }

    public String isSafeRoom(int[] roomNumbers) {
        String roomType = ROOM_WITH_TRAP;

        if (this.rule.isSafe(roomNumbers)) {
            roomType = SAFE_ROOM;
        }
        return roomType;
    }
}
