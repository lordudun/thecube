package com.lordudun.codingdojo.thecube;

public interface SafeDoorRule {

    boolean isSafe(int[] roomNumbers);
}
