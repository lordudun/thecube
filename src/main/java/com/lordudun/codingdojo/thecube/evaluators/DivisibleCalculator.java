package com.lordudun.codingdojo.thecube.evaluators;

class DivisibleCalculator {

    protected int calculateFirstDivisorNumber(int number) {

        boolean compositeNumber = false;
        int currentNumber = 1;
        int lastNumberToCheck = calculateLastNumberToCheck(number);

        while(currentNumber <= lastNumberToCheck && !compositeNumber) {

            currentNumber++;
            compositeNumber = isDivisibleBy(number, currentNumber);
        }

        return compositeNumber?currentNumber:1;
    }

    protected boolean isDivisibleBy(int dividend, int divisor) {
        int rest = dividend%divisor;
        return isExactDivision(rest);
    }

    protected int calculateSequentialExactDivision(int dividend, int divisor) {
        int quotient = dividend;
        int rest = 0;

        while(isExactDivision(rest) && quotient > 1) {
            rest = quotient%divisor;
            quotient = quotient/divisor;
        }
        return rest;
    }

    protected boolean isExactDivision(int rest) {
        return rest == 0;
    }

    private int calculateLastNumberToCheck(int number) {
        Double square = Math.sqrt(number);
        return square.intValue();
    }
}
