package com.lordudun.codingdojo.thecube.evaluators;

public class PrimePotencyEvaluator {

    private DivisibleCalculator divisibleCalculator;

    public PrimePotencyEvaluator() {
        this.divisibleCalculator = new DivisibleCalculator();
    }

    public boolean isPrimePotencyNumber(int number) {
        if (isNeutralNumber(number)) {
            return false;
        }
        return evaluateIfIsPrimePotency(number);
    }

    private boolean evaluateIfIsPrimePotency(int number) {
        int compositeNumber = divisibleCalculator.calculateFirstDivisorNumber(number);
        return isPrimePotencyNumberOf(number, compositeNumber);
    }

    private boolean isPrimePotencyNumberOf(int dividend, int prime) {

        int rest = 0;
        if (prime > 1) {
            rest = this.divisibleCalculator.calculateSequentialExactDivision(dividend, prime);
        }
        return this.divisibleCalculator.isExactDivision(rest);
    }

    private boolean isNeutralNumber(int number) {
        return number == 1;
    }
}
